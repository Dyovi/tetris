const coloresPiezas = {
    o : "#c5d",
    i : "#6f4",
    s : "#6fd",
    z : "#ef6",
    l : "#1a5",
    j : "#d42",
    t : "#632"
}
const tamanyoCasilla = 25
var juego = {
    tablero : [],
    estadoTablero : false,
    gameOver : false,
    puntuacion : 0,
    puntuacionMax : 0,
    piezaVigente : undefined,
    piezaSiguiente : undefined,
    piezasContadores : {
        i : 0,
        j : 0,
        l : 0,
        o : 0,
        s : 0,
        t : 0,
        z : 0
    },
    piezasPool : [
        [[[0,0,0,0],[0,1,1,0],[0,1,1,0],[0,0,0,0]],coloresPiezas.o,"o"],
        [[[0,1,0,0],[0,1,0,0],[0,1,0,0],[0,1,0,0]],coloresPiezas.i,"i"],
        [[[0,0,0,0],[0,1,1,0],[1,1,0,0],[0,0,0,0]],coloresPiezas.s,"s"],
        [[[0,0,0,0],[0,1,1,0],[0,0,1,1],[0,0,0,0]],coloresPiezas.z,"z"],
        [[[0,1,0,0],[0,1,0,0],[0,1,1,0],[0,0,0,0]],coloresPiezas.l,"l"],
        [[[0,1,1,0],[0,1,0,0],[0,1,0,0],[0,0,0,0]],coloresPiezas.j,"j"],
        [[[0,0,0,0],[1,1,1,0],[0,1,0,0],[0,0,0,0]],coloresPiezas.t,"t"]
    ],
    velocidad : 1000,
    nivel : 1,
    
    initJuego : function(){
        if (document.cookie.includes("mejorPuntuacion")){
            var valor = document.cookie.split("=")[1]
            this.puntuacionMax = valor
        }
        this.tablero = Array.from(Array(25), () => Array.from(Array(10), () => 0))
        this.siguientePieza()
    },
    teclas : function(e){
        if (this.gameOver == true) return
        switch (e.key) {
            case 'ArrowLeft': //Izquierda
                this.piezaVigente.moverIzquierda()
                pintarTodo()
                break
            case 'ArrowRight': //Derecha
                this.piezaVigente.moverDerecha()
                pintarTodo()
                break
            case 'ArrowDown': //Abajo
                if(this.piezaVigente.moverAbajo()){
                    juego.subirPuntuacion(1)
                    pintarTodo()
                }
                break
            case 'a': // Tecla A : Rotar izquierda
                if(this.piezaVigente.rotarDerecha()){
                    pintarTodo()
                }
                break
            case 'ArrowUp': // Tecla arriba : Rotar derecha
            case 'd': //Tecla D : Rotar derecha
                if (this.piezaVigente.rotarIzquierda()){
                    pintarTodo()
                }
                break
        }
    },
    getColor : function(letra){
        return this.coloresPiezas[letra]
    },
    movimientoAutomatico : function(){
        this.piezaVigente.moverAbajo()
    },
    siguientePieza : function(){
        if (this.piezaVigente == undefined){
            this.piezaVigente = this.getPiezaRandom()
        } else {
            this.piezaVigente = this.piezaSiguiente
            this.subirPuntuacion(2)         
            if (this.comprobarSubidaNivel()){
                this.subirNivel()
                this.subirPuntuacion(3)
            }   
        }
        this.piezasContadores[this.piezaVigente.letra]++
        if (this.comprobarSubidaNivel()){
            this.subirNivel()
            this.subirPuntuacion(3)
        }  
        this.piezaSiguiente = this.getPiezaRandom()
    },
    getPiezaRandom(){
        var arrPieza = this.piezasPool[Math.floor(Math.random()*this.piezasPool.length)]
        return new Pieza(arrPieza[0], arrPieza[1], 3, 0, arrPieza[2]);
    },
    casillaLibre : function(y, x){
        if (y >= 25) return false
        if (this.tablero[y][x] == 0) return true
        return false
    }, 
    fijarPieza(){
        this.piezaVigente.getForma().forEach((element, y) => {
            element.forEach((casilla, x) => {
                if (casilla == 1){
                    this.tablero[y+this.piezaVigente.y][x+this.piezaVigente.x] = this.piezaVigente.letra
                }
            });
        });
    },
    fin(){
        this.gameOver = true
    },
    subirPuntuacion(tipo){
        switch(tipo){
            case 1: //Subida por bajar la pieza con el teclado
                this.puntuacion++
                break
            case 2: //Subida por cada nueva pieza
                this.puntuacion += 10
                break
            case 3: //Subida por nuevo nivel
                this.puntuacion += 20
                break
        }
        if (this.puntuacion > this.puntuacionMax){
            this.puntuacionMax = this.puntuacion
            document.cookie = `mejorPuntuacion=${this.puntuacionMax}`
        }
    },
    comprobarSubidaNivel(){
        var contadorGlobal = 0
        for (const key in juego.piezasContadores) {
            contadorGlobal += juego.piezasContadores[key]
        }
        return contadorGlobal % 10 == 0;
    },
    subirNivel(){
        this.nivel++;
        this.velocidad = this.velocidad - (this.velocidad*0.1)
        clearInterval(partida)
        partida = intervalo(this.velocidad)
    }, 
    comprobarLinea(){
        this.tablero.forEach((element, y) => {
            if (!element.includes(0)){
                this.tablero.splice(y, 1)
                this.tablero.unshift(Array.from(Array(10), () => 0))
            }
        });
    }
}

var Pieza = function(forma, color, x, y, letra){
    this.forma = forma
    this.color = color
    this.x = x
    this.y = y
    this.letra = letra
    this.haPodidoAvanzar = false
}

Pieza.prototype.moverIzquierda = function(){
    var puedeMoverIzquierda = true

    this.getForma().forEach((element, y) => {
        element.forEach((casilla, x) => {
            if (casilla == 1){
                if (!juego.casillaLibre(y+this.y, x+this.x-1)){
                    puedeMoverIzquierda = false
                }
            }
        });
    });

    if (puedeMoverIzquierda){
        this.x--
        return true
    }
}

Pieza.prototype.moverDerecha = function(){
    var puedeMoverDerecha = true

    this.getForma().forEach((element, y) => {
        element.forEach((casilla, x) => {
            if (casilla == 1){
                if (!juego.casillaLibre(y+this.y, x+this.x+1)){
                    puedeMoverDerecha = false
                }
            }
        });
    });

    if (puedeMoverDerecha){
        this.x++
        return true
    }
}

Pieza.prototype.moverAbajo = function(esBajadaManual){ 
    var puedeMoverAbajo = true
    this.getForma().forEach((element, y) => {
        element.forEach((casilla, x) => {
            if (casilla == 1){
                if (!juego.casillaLibre(y+this.y+1, x+this.x)){
                    puedeMoverAbajo = false
                }
            }
        });
    });
    
    if (puedeMoverAbajo){
        this.y++
        this.haPodidoAvanzar = true
        return true
    } else {
        if (this.haPodidoAvanzar == false) {
            juego.fin()
        } else {
            juego.fijarPieza()
            juego.comprobarLinea()
            juego.siguientePieza()
        }
    }
}

Pieza.prototype.rotarDerecha = function(){
    var formaNova = new Array();
    var puedeRotar = true
    for (var i=0;i<this.forma.length;i++) {
        formaNova[i]=new Array();
        for (var j=0;j<this.forma[i].length;j++) {
            formaNova[i][j]=this.forma[this.forma[i].length-1-j][i];
        }
    }

    formaNova.forEach((element, y) => {
        element.forEach((casilla, x) => {
            if (casilla == 1){
                if (!juego.casillaLibre(y+this.y, x+this.x)){
                    puedeRotar = false
                }
            }
        });
    });

    if (puedeRotar){
        this.forma = formaNova;
        return true
    }
}

Pieza.prototype.rotarIzquierda = function(){
    var cantidadRotacionesCorrectas = 0
    for (let i = 0; i < 3; i++) {
        if (this.rotarDerecha()) cantidadRotacionesCorrectas++
    }
    if (cantidadRotacionesCorrectas == 3){
        return true
    }
}

Pieza.prototype.getForma = function(){
    return this.forma
}

// JUEGO

juego.initJuego()

var partida = intervalo(juego.velocidad)

function intervalo(vel){
    return setInterval(() => {
        juego.movimientoAutomatico()
        pintarTodo()
        if (juego.gameOver == true) {
            pintarGameOver()
            clearInterval(partida)
        }
    }, vel)
}

function pintarTodo(){
    pintarZonaJuego()
    pintarPieza()
    pintarPiezaSiguiente()
}

function pintarZonaJuego(){
    var canvas = document.getElementById("canvas")
    var ctx = canvas.getContext("2d")

    $("#puntuacion").text(`Puntuación: ${juego.puntuacion}`)
    $("#record").text(`Récord: ${juego.puntuacionMax}`)
    $("#nivel").text(`Nivel: ${juego.nivel}`)

    //Se limpia el canvas para evitar que se pegue encima del anterior
    ctx.clearRect(0, 0, canvas.width, canvas.height)

    juego.tablero.forEach((element, y) => {
        element.forEach((celda, x) => {
           
            switch(celda){
                case 0: //Vacío
                    ctx.globalCompositeOperation = 'source-over'                    
                    //Dibuja la casilla con el color pertinente
                    ctx.strokeStyle = "black"
                    //Se multiplica la x e y por el tamaño de cada casilla, para que esté colocada en la posición correcta.
                    ctx.strokeRect(x*tamanyoCasilla, y*tamanyoCasilla, tamanyoCasilla, tamanyoCasilla)
                    ctx.stroke()
                    break
                default: //Piezas
                    ctx.globalCompositeOperation = 'destination-over'                    
                    ctx.strokeStyle = "black"
                    ctx.strokeRect(x*tamanyoCasilla, y*tamanyoCasilla, tamanyoCasilla, tamanyoCasilla)
                    ctx.fillStyle = coloresPiezas[celda]
                    ctx.stroke()
                    ctx.fillRect(x*tamanyoCasilla, y*tamanyoCasilla, tamanyoCasilla, tamanyoCasilla)
                    break
            }
            
        });
    });

    //Para mostrar el tablero
    $("#juego").css("display","");
}

function pintarPieza(){
    var canvas = document.getElementById("canvas")
    var ctx = canvas.getContext("2d")

    juego.piezaVigente.getForma().forEach((element, y) => {
        element.forEach((casilla, x) => {
            if (casilla == 1){
                ctx.globalCompositeOperation = 'destination-over'                    
                //ctx.strokeStyle = "black"
                ctx.fillStyle = juego.piezaVigente.color
                //ctx.strokeRect(x*tamanyoCasilla, (y+juego.piezaVigente.y)*tamanyoCasilla, tamanyoCasilla, tamanyoCasilla)
                ctx.fillRect((x+juego.piezaVigente.x)*tamanyoCasilla, (y+juego.piezaVigente.y)*tamanyoCasilla, tamanyoCasilla, tamanyoCasilla)
                ctx.stroke()
            }
        });
    });
}

function pintarPiezaSiguiente(){
    var canvas = document.getElementById("piezaSiguiente")
    var ctx = canvas.getContext("2d")
    ctx.clearRect(0, 0, canvas.width, canvas.height)

    juego.piezaSiguiente.getForma().forEach((element, y) => {
        element.forEach((casilla, x) => {
            if (casilla == 1){
                ctx.globalCompositeOperation = 'destination-over'                    
                ctx.strokeStyle = "black"
                ctx.fillStyle = juego.piezaSiguiente.color
                ctx.strokeRect(x*tamanyoCasilla, y*tamanyoCasilla, tamanyoCasilla, tamanyoCasilla)
                ctx.fillRect(x*tamanyoCasilla, y*tamanyoCasilla, tamanyoCasilla, tamanyoCasilla)
                ctx.stroke()
            }
        });
    });
}

function pintarGameOver(){
    var canvas = document.getElementById("canvas")
    var ctx = canvas.getContext("2d")

    ctx.globalCompositeOperation = 'source-over'

    ctx.fillStyle = "lightgray"
    ctx.fillRect(50,75,6*tamanyoCasilla,5*tamanyoCasilla)

    ctx.font = "40px Arial"
    ctx.fillStyle = '#111'
    ctx.textAlign = 'center'
    ctx.fillText("GAME", 125, 125)
    ctx.fillText("OVER", 125, 175)
}